<?php
session_start();
?>


<!DOCTYPE html>
<html Lang="DE">
<head>
    <meta charset="utf-8">
    <title>Tic-Tac-Toe. This is the title. It is displayed in the titlebar of the window in most browsers.</title>
    <meta name="description" content="Tic-Tac-Toe-Game. Here is a short description for the page. This text is displayed e. g. in search engine result listings.">
    <style>
        table.tic td {
            border: 1px solid #333; /* grey cell borders */
            width: 8rem;
            height: 8rem;
            vertical-align: middle;
            text-align: center;
            font-size: 4rem;
            font-family: Arial;
        }
        table { margin-bottom: 2rem; }
        input.field {
            border: 0;
            background-color: white;
            color: white; /* make the value invisible (white) */
            height: 8rem;
            width: 8rem !important;
            font-family: Arial;
            font-size: 4rem;
            font-weight: normal;
            cursor: pointer;
        }
        input.field:hover {
            border: 0;
            color: #c81657; /* red on hover */
        }
        .colorX { color: #e77; } /* X is light red */
        .colorO { color: #77e; } /* O is light blue */
        table.tic { border-collapse: collapse; }
    </style>
</head>
<body>
    <section>
        <h1>Tic-Tac-Toe</h1>
        <article id="mainContent">
            <h3>how to play this game:</h3>
            <p>First Player got the Symbol "X"; Second Player the Symbol "O"</p>
			<p>Winner is the person who gets 3 symbols in a row (horizontal, diagonal, vertical)</p>
            <form method="get" action="index.php">
<!--PHP Starts here-->
<?php
	

//The following methods are not yet working because it is not yet clear where I can retrieve them - > further function to retrieve them has yet to be created

/* function getWinner() {

        if ($winner = $board->checkHorizontal()) {
            return $winner;
        }

        if ($winner = $board->checkVertical()) {
            return $winner;
        }

        if ($winner = $board->checkDiagonal()) {
            return $winner;
        }
    }	
function isGameCompleted()
{
	
}
function checkHorizontal() {
	for ($i = 0; $i < count($board); $i++)
	{
		$winner = board[$i][0];
		for ($j = 0; $j < count($board[$i]); $j++)
		{
			if ($board[$i][$j] != $winner) {
				$winner = null;
				break;
			}
		}
	}
}

function checkVertical() {
		for ($i = 0; $i < count($board); $i++){
			$winner = $board[0][$i];

		for ($j = 0; $j < count($board[$i]); $j++) 
		{
			if ($board[$j][$i] != $winner) {
				$winner = null;
				break;
			}
		}
		// if the winner is not null stop
		if ($winner !== null) {
			break;
		}
	}
	return $winner;
}

function checkDiagonal() {
  
	// check the first diagonal
	$winner = $board[0][0];
	for ($i = 0; $i < count($board); $i++){
		if ($board[$i][$i] != $winner) {
			$winner = null;
			break;
		}
	}

	// if there's no winner check the second diagonal
	if ($winner === null) {
		$winner = $board[0][2];
		for ($j = 0; $j < count($board[$i]); $j++){
			
			if ($board[$i][2-$i] != $winner) {
				$winner = null;
				break;
			}
		}
	}
	return $winner;
} */


$player = "X";

		$board = array();
		$board = array(
					array("","",""),
					array("","",""), 
					array("","","")
					);




//The session is tied to the board		
if (isset($_SESSION['tictactoe'])){
		$board = $_SESSION['tictactoe'];

	}
if (isset($_GET["reset"]) ){
	session_destroy();
	header("Location: index.php");
}	

// double for - loop for setting the marks and changing player
for ($i = 0; $i < count($board); $i++)
{
	for ($j = 0; $j < count($board[$i]); $j++)
	{
        if(isset($_GET["cell-".$i."-".$j]) ){
            $board[$i][$j] = $_GET["cell-".$i."-".$j];
            if ($_GET["cell-".$i."-".$j] == "X") {
                $player = "O";
            }else{
                $player = "X";
            }
		
        }
	} 
}


$_SESSION["tictactoe"] = $board;

// Building the TicTacToe Board
echo '<table class="tic">'."\n";			
for ($i = 0; $i < count($board); $i++)
	{
	echo '<tr>'."\n";
	for ($j = 0; $j < count($board[$i]); $j++)
		{		
			if ($board [$i][$j] === "")
			{
				echo "\t".'<td><input type="submit" class="field" name="cell-'.$i.'-'.$j.'"value='.$player.'></td>'."\n";
			}
			else
			{
				echo "\t".'<td><span class="color'.$board[$i][$j].'">'.$board[$i][$j].'</span></td>'."\n";
			}
		} 	
	echo '</tr>';	
	}
?>
<!--PHP Ends here-->
                </table>
				<input type="submit" name="reset" value="reset"></input>
				</form>
			</article>
		</section>
	</body>
</html>